var struct_data_header__t =
[
    [ "bIMUType", "struct_data_header__t.html#a8b7335a2223b09febdfd7610bbba428f", null ],
    [ "bLIDARType", "struct_data_header__t.html#a5f71460e5dacf5122ec804504d5b5f86", null ],
    [ "cOperatorName", "struct_data_header__t.html#abe8bd9b50289041be6b996d445885a71", null ],
    [ "cPreamble", "struct_data_header__t.html#aecf351e7cfb3ff30a88c60e2a1852bf8", null ],
    [ "cProjectName", "struct_data_header__t.html#a10915b04bfc79991bf73b6a7993066d7", null ],
    [ "endTime", "struct_data_header__t.html#a4d95c01e1ccc903f86170b6c613b676d", null ],
    [ "res", "struct_data_header__t.html#ab9b5f030b9f147af07518a302be3b5e7", null ],
    [ "startTime", "struct_data_header__t.html#ad33c39e0d1926b3bf18005d4c5d26a41", null ],
    [ "ulPrevJobLastSector", "struct_data_header__t.html#a6b33eb05add6544381553ce8f6b37fc0", null ],
    [ "ulTotalSectors", "struct_data_header__t.html#adbdbd9ca2a569fe44ae4945c6172e3bc", null ],
    [ "ulValidity", "struct_data_header__t.html#a3c4003047e44f46bc75ba1ee67625c19", null ]
];