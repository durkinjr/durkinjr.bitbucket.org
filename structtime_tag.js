var structtime_tag =
[
    [ "wDay", "structtime_tag.html#a8d54eea0615475ed61610caac80f2664", null ],
    [ "wDayOfWeek", "structtime_tag.html#ad3da1542e838cf8ec0bf0a568ed407c0", null ],
    [ "wHour", "structtime_tag.html#a485c827ce5f895b5e9ed9caa227aefd8", null ],
    [ "wMilliseconds", "structtime_tag.html#a70a30b0f0492154968b820238f8385f9", null ],
    [ "wMinute", "structtime_tag.html#a4522b7fc366818e9e48522d919e1e9e3", null ],
    [ "wMonth", "structtime_tag.html#a63a7b6f9d7071ac3161cde29de1a9594", null ],
    [ "wSecond", "structtime_tag.html#a92eeb021538adaaff8e11e8e990105ec", null ],
    [ "wYear", "structtime_tag.html#a6ce4ac6eb1c47986f9bbd7b9707b26fa", null ]
];