var struct_seq_info_def =
[
    [ "ActualRange", "struct_seq_info_def.html#a15d087abcd215e9040cd39ab29c335d4", null ],
    [ "AttenuatorSetting", "struct_seq_info_def.html#a5f63d255ad1b7191cd5bf9e829b70bf0", null ],
    [ "Column", "struct_seq_info_def.html#aafcfdbf4b5137eab3d2a60d1cdf49e03", null ],
    [ "DAC", "struct_seq_info_def.html#a5b5a07292679f765bd4adebfb34b9e68", null ],
    [ "Row", "struct_seq_info_def.html#aa80b461518ff805d9352b5dc132599a0", null ],
    [ "TempA", "struct_seq_info_def.html#a743ec091eaf1724760e33a09079db977", null ],
    [ "x1", "struct_seq_info_def.html#af7aaa2ec9887a86f3e94ef933491c8b6", null ],
    [ "x2", "struct_seq_info_def.html#aca9a18f50af925d56944d20c781572d2", null ],
    [ "y1", "struct_seq_info_def.html#a9b86ebe6b40308284b4b70fc819eed95", null ],
    [ "y2", "struct_seq_info_def.html#ad8274ed0cc8a2eed973f1cb5f686cf6a", null ]
];