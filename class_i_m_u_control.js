var class_i_m_u_control =
[
    [ "IMUControl", "class_i_m_u_control.html#ae1b0a4b2ea0b7b7f3466925a7158b478", null ],
    [ "~IMUControl", "class_i_m_u_control.html#ab423a2adedf6b1bc576bd4ad1f3c5929", null ],
    [ "getLastLatLong", "class_i_m_u_control.html#a1f54de8b4e2b18c9cde1ca07ad415324", null ],
    [ "getLastQuaternion", "class_i_m_u_control.html#a53bededb19653272ffc2cb12369d5934", null ],
    [ "getLastVelocity", "class_i_m_u_control.html#af643a7f15dd5faa4724c85d46b0ec15f", null ],
    [ "getLastYawRollPitch", "class_i_m_u_control.html#ad04e7041beb83e84c57ac6187c639673", null ],
    [ "getNextDataPacket", "class_i_m_u_control.html#a252d5a206edff77ce7602b5cf9adc395", null ],
    [ "init", "class_i_m_u_control.html#aeea2f63602249533b2fe93b4bda43318", null ]
];