var indexSectionsWithContent =
{
  0: "_abcdefghijlmnopqrstuvwxy~",
  1: "acdilmpstvw",
  2: "a",
  3: "ahilmrstvw",
  4: "abcdefgilmnopqrstvw~",
  5: "abcdefhijlmnopqrstuvwxy",
  6: "bcdrst",
  7: "c",
  8: "afrs",
  9: "_fhip"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Macros"
};

